console.log("hello from JS"); //MESSAGE

//[SECTION] CONTROL STRUCTURES


//[SUB SECTION]: IF-ELSE STATEMENT

let numA = 3; //we will try to assess the value.

//If statement (Branch)
//The task of the if statement is to execute a procudure/action if the specified condition is "true".
if (numA <= 0) {
	//truthy branch
	//this block of code will run if the condition is MET.
	console.log('The condition was MET!'); 
}

//let name = 'Lorenzo'; //Allowed
//Maria => NOt allowed

let isLegal = false; 
//! -> NOT //false

//create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (isLegal) {
	//if the passess the condition the "truthy" branch will run
	console.log('User can proceed!');
}

//[SUB SECTION] Else statement

// => This executed a statement if ALL other conditions are "FALSE" and/or has failed.

//lets create a control structure that will allow us to simulate a user login.


//prompt box -> prompt(): this will allow us to display a prompy dialog box which we can the user for an input.

//syntax: prompt(text/message[REQUIRED], placeholder/default text[OPTIONAL]);
//prompt("Please enter your first name:", "Martin"); 

//lets create a control structure that will allow us to check of the user is old enough to drink,
let age = 21; 

//alert() => display a message box to the user which would require their attention
// if (age >= 18) {
// 	// "truthy" brach
// 	alert("Your old enough to drink");
// } else {
// 	//"falsy" branch
// 	alert("Come back another day!");
// }


// lets create a control structure that will ask for the number of drinks that you will order. 

//ask the user how many drinks he wants
// let order = prompt('How many orders of drinks do you like?');

//convert the string data type into a number.
//parseInt() => will allow us to convert strings into integers

//create a logic that will make sure that the user's input is greater than 0

//Type coercion => conversion data type was converted to another data type.

//composite -> multiple data 
//primitive -> single data 

//1. if one of the operands in an object, it will be converted into a primitive data type (str, number, boolean).
//2. if atleast 1 operand is a string it will convert the other operand into a string as well. 
//3. if both numbers are numbers then an arithmetic operation will be executed.

//in JS there 3 ways to multiply a string. 
   //1. repeat() method => this will allow us to return a new string value that contains the number of copies of the string. 
      //syntax: str.repeat(value/number)

   //2. loops => for loop
   //3. loops => while loop method.

// x + y 
// if (order > 0) {
//   console.log(typeof order);
//   let cocktail = "🍸"; 
//   alert(cocktail.repeat(order));
//   //alert(order * "🍸"); 
//     //multiplication	
// } else {
// 	alert('The number should be above 0');
// }

//You want to create other predetermined conditions you can create a nested (multiple) if-else statement. 


//15 mins to formulate a logic that will allow us to multiply a string to a desired number.


// Mini task for vaccine checker
function vaccineChecker () {
  // ask information from the user
  let vax = prompt('What brand is your vaccine?');
  // we need to process the input of the user so that what ever input he will enter we can control the uniformity of the character casing.
  // toLowercase() - will allow us to convert a string into all lower case characters.
  vax = vax.toLowerCase();
  // create a logic that will allow us  to check the values inserted by the user to match given parameters
  if (vax === 'pfizer' || vax === 'moderna' || vax === 'astra zenica' || vax === 'astrazenica' || vax === 'janssen') {
    alert(vax  +  ' is allowed to travel');
  } else {
    alert('sorry not permitted');
  }
}

//onclick =>is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure. 

//syntax: <element/component onclick="myScript/Function" >

// MINI TASK TYPHOON CHECKER

function typhoonChecker() {
  // going to neeed an input from the user.
  // we need a number data type so that we can properly compare the values.
  let windspeed = parseInt(prompt('Wind Speed:'));
  if (windspeed <= 29 ) {
    alert('Not a Typhoon yet!');
  } else if (windspeed >= 30 && windspeed <= 61 ) {
    alert('Tropical Depression Detected');
  } else if (windspeed >= 62 && windspeed <= 88) {
    alert('Tropical Storm Detected');
  } else if (windspeed >= 89 && windspeed <= 117) {
    alert('Severe Tropical Storm Detected');
  }
  else  {
    alert('Typhoon Detected');
  }
}

// conditional ternary

function ageChecker() {
 let age = parseInt(prompt('How old are you?'));
 // i'm going to simplify our structure below using a ternary operator

// ternary structure is a short hand version of if else.
 return (age >= 18) ? alert('Old enough to vote') : alert('Not yet old enough');

 // if (age >= 18 ) {
 //  alert('Old enough to vote');
 // } else {
 //  alert('Not yet old enough');
 // }
}

// Switch statements

function determineComputerOwner() {
  let unit = prompt('What is the unit no. ?');

  let manggagamit;

  // The unit -> represents the unit number.
  // The manggagamit -> represent the user who owns the unit.
  switch (unit) {
    // declare multiple cases to represent each outcome that will match the value inside the expression.
    case '1':
      manggagamit = "John Travolta";
      alert('This unit is owned by John Travolta');
      break;
    case '2':
      manggagamit = "Steve Jobs";
      alert('This unit is owned by Steve Jobs');
      break;
    case '3':
      manggagamit = "Pablo Escobar";
      alert('This unit is owned by Pablo Escobar');
      break;
    case '4':
      manggagamit = "Son Goku";
      alert('This unit is owned by Son Goku');
      break;
    case '5':
      manggagamit = "Monkey D Luffy";
      alert('This unit is owned by Monkey D Luffy');
      break;
    default:
      manggagamit = 'wala yan sa options na pagpipilian';
      alert('wala yan sa options na pagpipilian');
      //if all else fails or if non of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
  }
}