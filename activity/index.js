console.log("Hello World!!!");


function getNumber () {
	let numOne = parseInt(prompt('Provide a number'));
	let numTwo = parseInt(prompt('Provide another number'));

	let sum = numOne + numTwo;

	if(sum < 10) {
		console.warn('The sum of the two numbers are: ' + sum);
	} else if (sum <= 20) {
		alert('The difference of the two numbers are: ' + (parseInt(numOne) - parseInt(numTwo)));
	} else if (sum <= 29) {
		alert('The product of the two numbers are: ' + (parseInt(numOne) * parseInt(numTwo)));
	} else if(sum >= 30) {
		alert('The quotient of the two numbers are: ' + (parseInt(numOne) / parseInt(numTwo)));
	}
}


function getDetails () {
	let name = prompt('What is your name?')
	console.log(name);
	let age = prompt('Your age?')

	if(name === '' || age === '' || name === null || age === null) {
		alert('Are you a time traveler?')
	} else if(name !== '' && age !== '') {
		alert("Hello " + name + " Your age is " + age);
	}
}



function determineAge () {
	let userAge = parseInt(prompt('Enter your age: '));

	switch (userAge) {
		case 18:
			alert('You are now allowed to party');
			break
		case 21:
			alert('You are now part of the adult society');
			break
		case 65:
			alert('We thank you for your contribution to society');
			break
		default:
			alert("Are you sure you're not an alien?");
	}
}


function ageChecker() {
	let userInput = document.getElementById('age').value;

	let message = document.getElementById('outputDisplay');

	try {
		if (userInput === '') throw 'The input is empty';
		if (isNaN(userInput)) throw 'The input is not a number';
		if (userInput <= 0) throw 'Not a valid input';
		if (userInput <= 7) throw 'The input is good for preschool';
		if (userInput > 7) throw 'Too old for preschool';
	} catch (err) {
		message.innerHTML = "Age Input: " + err;
	} finally {
		alert('this is from the finally section');
	}
}